import java.util.*;
public class Busqueda{

  public Puzzle otraves(Nodo actual){

    Vector<Nodo> abiertos = new Vector<Nodo>(1,1);

    Map<String, Nodo> abiertosMap = new HashMap<String, Nodo>();
    Map<String, Nodo> cerradosMap = new HashMap<String, Nodo>();

    abiertos.add(actual);
    abiertosMap.put( actual.toString(), actual );

    boolean res = false;

    while(!res  && !abiertos.isEmpty()){

      actual = abiertos.elementAt(0);

      if( actual.isObjetive() ){ // Si no es la solucion
        res = true;
      }else{
        //Eliminamos el primer elemento del vector
        abiertos.remove(0);
        // Quitamos la llave de actual en abiertosMap
        abiertosMap.remove(actual.toString());
        // Agregamos la llave de actual en cerradosMap
        cerradosMap.put(actual.toString(), actual);


        // Sacamos todos los hijos de el nodo actual.
        Vector<Nodo> hijos = actual.getChildren();
        // Recorremos todos los hijos.
        for(int i=0; i< hijos.size()-1; i++ ){

          // Si el elemento en el index i no esta en la lista de cerrados.
          if(!cerradosMap.containsKey( hijos.elementAt(i).toString() )  ){

            int valorActual = actual.getValue();

            if(!abiertosMap.containsKey( hijos.elementAt(i).toString() )){
              abiertos.add( hijos.elementAt(i) );
              abiertosMap.add( hijos.elementAt(i).toString(), hijos.elementAt(i) );
              camino.add(hijos.elementAt(i));
            }else
              if( valorActual < hijos.elementAt(i).getValue() ){
                abiertosMap.remove(hijos.elementAt(i));
                
              }// End if

          }// End if

        }// End for
      }

    }

  }
}
