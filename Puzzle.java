import java.util.*;
public class Puzzle{
	private int matriz[][];
	private int columnaCero;
	private int filaCero;
	private int fin[][] =	{				// Matriz final xD
											{1,2,3},
											{4,5,6},
											{7,8,0}
						};
	public Puzzle(int[][] matriz){
		this.matriz 		= matriz;
		whereIsZero();
	}

	private void whereIsZero(){
		for(int i = 0; i<3; i++){
			for(int j = 0; j < 3 ; j++ ){
				if(matriz[i][j] == 0){
					filaCero 	= i;
					columnaCero = j;
					break;
				}
			}
		}
	}
	public int[][] getMatriz(){
		return matriz;
	}

	public int getFilaCero(){
		return filaCero;
	}

	public int getColumnaCero(){
		return columnaCero;
	}
	public Puzzle clone(){
		int [][]m ={
						{matriz[0][0],matriz[0][1],matriz[0][2]},

						{matriz[1][0],matriz[1][1],matriz[1][2]},

						{matriz[2][0],matriz[2][1],matriz[2][2]}

					};
		return new Puzzle(m);
	}
	public boolean equals(Puzzle input){
		int m2[][] = input.getMatriz();
		for(int i = 0; i<matriz.length; i++){

			if(m2[i][0] != matriz[i][0])
				return false;

			if(m2[i][1] != matriz[i][1])
				return false;

			if(m2[i][2] != matriz[i][2])
				return false;
		}
		return true;
	}

	public boolean isObjetive(){
		int m2[][] = matriz;
		for(int i = 0; i<fin.length; i++){

			if(m2[i][0] != fin[i][0])
				return false;

			if(m2[i][1] != fin[i][1])
				return false;

			if(m2[i][2] != fin[i][2])
				return false;
		}
		return true;
	}
	public String toString() {
 		String salida = "";

 		salida += "\n[" + matriz[0][0] + "] [" + matriz[0][1] + "] ["+ matriz[0][2] + "]";
 		salida += "\n[" + matriz[1][0] + "] [" + matriz[1][1] + "] ["+ matriz[1][2] + "]";
 		salida += "\n|" + matriz[2][0] + "] [" + matriz[2][1] + "] ["+ matriz[2][2] + "]";

 		return salida;
	}

	public void moveDown(){
		int aux 						= matriz[filaCero-1][columnaCero];
		matriz[filaCero][columnaCero] 	= aux;
		filaCero--;
		matriz[filaCero][columnaCero]	= 0;
	}

	public void moveUp(){
		int aux 						= matriz[filaCero+1][columnaCero];
		matriz[filaCero][columnaCero] 	= aux;
		filaCero++;
		matriz[filaCero][columnaCero]	= 0;
	}

	public void moveRight(){
		int aux 						= matriz[filaCero][columnaCero-1];
		matriz[filaCero][columnaCero] 	= aux;
		columnaCero--;
		matriz[filaCero][columnaCero]	= 0;
	}

	public void moveLeft(){
		int aux 						= matriz[filaCero][columnaCero+1];
		matriz[filaCero][columnaCero] 	= aux;
		columnaCero++;
		matriz[filaCero][columnaCero]	= 0;
	}

	public Vector<Puzzle> getStates(){
		Vector<Puzzle> v 	= new Vector<Puzzle>(1,1);

		Puzzle a 	 		= this.clone();
		Puzzle b 	 		= this.clone();
		Puzzle c 	 		= this.clone();
		Puzzle d 	 		= this.clone();

		if( filaCero 		== 1 ){
			c.moveUp();
			d.moveDown();
			v.add(c);
			v.add(d);
		}
		if(columnaCero 		== 0){
			a.moveLeft();
			v.add(a);
		}
		if( columnaCero 	== 2 ){
			a.moveRight();
			v.add(a);
		}

		if( filaCero 		== 0){
			c.moveUp();
			v.add(c);
		}
		if( columnaCero 	== 1){
			a.moveRight();
			b.moveLeft();
			v.add(a);
			v.add(b);
		}
		if(filaCero 		== 2){
			c.moveDown();
			v.add(c);
		}
		return v;
	}

	public int getValue(){
		int r = 0;

		for(int i = 0; i<fin.length; i++){

			if(matriz[i][0] != fin[i][0])
				r++;

			if(matriz[i][1] != fin[i][1])
				r++;

			if(matriz[i][2] != fin[i][2])
				r++;
		}
		return r;
	}
}
