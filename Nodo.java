import java.util.*;
public class Nodo{
	private Vector<Nodo> hijos = new Vector<Nodo>(1,1);
	private Puzzle nodo;
	private boolean visitado   = false;

	public Nodo(Puzzle nodo){
		this.nodo 	= nodo;
		// Se encapsula el Puzzle en un nodo.
		Vector<Puzzle> v = new Vector<Puzzle>(1,1);
		for(int i=0;i<v.size()-1; i++)
			hijos.add(new Nodo(v.elementAt(i)));
	}

	public void setSon(Nodo nodo){
		hijos.add(nodo);
	}

	public Vector<Nodo> getChildren(){
		return hijos;
	}

	public Puzzle getNodo(){
		return nodo;
	}

	public boolean getVisitado(){
		return visitado;
	}

	public void setVisitado(boolean flag){
		visitado = flag;
	}

	public String toString(){
		return nodo.toString();
	}

	public int getValue(){
		return nodo.getValue();
	}
	public boolean isObjetive(){
		return nodo.isObjetive();
	}
}
